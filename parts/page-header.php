<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//text
	$text = get_field('page_text');

	//product img
	$product = get_field('page_product');

	//product img
	$bag = get_field('page_bag');
?>
<?php if (is_front_page() ) : ?>
<section class="page__hero">
<?php else : ?>
<section class="page__hero page__hero--page">
<?php endif; ?>
	<div class="wrap hpad page__container">
		<div class="row page__row">

			<?php if (is_front_page() ) : ?>
			<div class="col-sm-11 col-sm-offset-1">
			<?php else : ?>
			<div class="col-sm-6 col-sm-offset-1">
			<?php endif; ?>
				<h1 class="page__title"><?php echo esc_html($title); ?></h1>
				<p class="page__text wow fadeInLeft"><?php echo $text; ?></p>

				<?php if (is_front_page() ) : ?>
				<img class="page__product" src="<?php echo esc_url($product['url']); ?>" alt="<?php echo esc_attr($product['alt']); ?>">
				<?php endif; ?>
		
			</div>

			<?php if (!is_front_page() ) : ?>
			<div class="col-sm-5 page__img">
				<img class="page__product--side" src="<?php echo esc_url($product['url']); ?>" alt="<?php echo esc_attr($product['alt']); ?>">
			</div>
			<?php endif; ?>

		</div>
	</div>
</section>